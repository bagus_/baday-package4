# private repo inside private repo #

buat terlebih dahulu package & repositories di bitbucket 

[cara membuat package](https://bitbucket.org/bagus_/baday-second)

* [package 4](https://bitbucket.org/bagus_/baday-package4/overview)
* [package 3](https://bitbucket.org/bagus_/baday-package3/overview)

pada [package 4](https://bitbucket.org/bagus_/baday-package4/overview) tambahkan code di *composer.json* untuk memanggil [package 3](https://bitbucket.org/bagus_/baday-package3/overview) , example

```
#!php

{
     .
     .
     .
     .
     .
    "require": {

        "baday/package3": "1.0"
    },
     "repositories": [
        {
            "type": "vcs",
            "url": "https://bagus_@bitbucket.org/bagus_/baday-package3.git"
        }
    ]
     .
     .
     .
     .
}

```

## Next Step ##
### usage ###
karena ini adalah package private jadi harus menambahkan repositories di file *composer.json*, code repositories tsb mengarah ke [package 4](https://bitbucket.org/bagus_/baday-package4/overview)

**example :**

```
#!php

  "repositories": [
        {
            "type": "vcs",
            "url": "https://bagus_@bitbucket.org/bagus_/baday-package4.git"
        }
    ],
```

pada terminal jalankan perintah berikut untuk install [package 4](https://bitbucket.org/bagus_/baday-package4/overview)

![p3.PNG](https://bitbucket.org/repo/e497a5/images/757467977-p3.PNG)